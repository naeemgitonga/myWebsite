import {
  APIGatewayProxyResult,
  APIGatewayProxyStructuredResultV2,
} from 'aws-lambda';

export default function response(
  body: unknown,
  statusCode: number,
  err: Error = undefined,
  headers: APIGatewayProxyStructuredResultV2['headers'] = {},
  multiValueHeaders = {}
): Promise<APIGatewayProxyResult> {
  const headerObj = {
    ...(statusCode >= 200 && statusCode < 300
      ? {
          'Access-Control-Allow-Origin': process.env.ORIGIN,
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Methods': 'POST,OPTIONS',
          'Access-Control-Allow-Credentials': 'true',
          'Content-Type': 'application/json',
        }
      : {}),
    ...headers,
  };
  const bodyObj = err
    ? {
        error: {
          name: err.name,
          message: err.message,
          stack: err.stack,
        },
      }
    : body;
  return Promise.resolve({
    body: JSON.stringify(bodyObj),
    headers: headerObj,
    isBase64Encoded: false,
    statusCode,
    multiValueHeaders,
  });
}
