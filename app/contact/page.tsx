import ContactForm from '@/components/ContactForm/ContactForm';
import Footer from '@/components/Footer/Footer';
import '../globals.css';

export default function Contact() {
  return (
    <>
      <ContactForm />
      <Footer />
    </>
  );
}
