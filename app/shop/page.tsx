import Footer from 'components/Footer/Footer';
import ShopView from 'components/ShopView/ShopView';
import '../globals.css';

export default function Books(): JSX.Element {
  return (
    <>
      <ShopView />
      <Footer />
    </>
  );
}
