import Footer from 'components/Footer/Footer';
import '../globals.css';
import CartVeiw from 'components/CartView/CartView';

export default function Cart(): JSX.Element {
  return (
    <>
      <CartVeiw />
      <Footer />
    </>
  );
}
