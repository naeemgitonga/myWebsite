import { Product } from '@/types/product';
import { useState, useCallback, useEffect } from 'react';
import { Subject } from 'rxjs';

export const itemCountUpdated = new Subject<number>();

interface CartItem extends Product {
  quantity: number;
}

export default function useCart(): [
  (arg: Product) => void,
  CartItem[],
  (id: number) => void,
  () => void,
] {
  const [cart, setCart] = useState<CartItem[]>([]);
  const updateCartAndStorage = (newCart: CartItem[]) => {
    window.localStorage.setItem('cart-jng', JSON.stringify(newCart));
    const quantity = newCart.reduce((prev, curr) => prev + curr.quantity, 0);
    const newCount = quantity > 99 ? 99 : quantity;
    itemCountUpdated.next(newCount);
  };

  useEffect(() => {
    const savedCart = window.localStorage.getItem('cart-jng');
    const parsedCart = savedCart ? JSON.parse(savedCart) : [];
    setCart(parsedCart as CartItem[]);
    updateCartAndStorage(parsedCart);
  }, []);

  const addItem = useCallback((item: Product) => {
    setCart((currentCart) => {
      const existingItemIndex = currentCart.findIndex(
        (existingItem) => item.id === existingItem.id
      );
      let updatedCart = [...currentCart];
      const itemToCopy = { ...item };

      if (existingItemIndex >= 0) {
        const updatedItem = {
          ...updatedCart[existingItemIndex],
          quantity: updatedCart[existingItemIndex].quantity + 1,
        };
        updatedCart[existingItemIndex] = updatedItem;
      } else {
        updatedCart = [...updatedCart, { ...itemToCopy, quantity: 1 }];
      }
      window.localStorage.setItem('cart-jng', JSON.stringify(updatedCart));
      updateCartAndStorage(updatedCart);
      return updatedCart;
    });
  }, []);

  const removeItem = useCallback((id: number) => {
    setCart((currentCart) => {
      const existingItemIndex = currentCart.findIndex((item) => item.id === id);
      if (existingItemIndex < 0) {
        // Item not found in the cart, return the current cart
        return currentCart;
      }

      let updatedCart = [...currentCart];
      const updatedItem = {
        ...updatedCart[existingItemIndex],
        quantity: updatedCart[existingItemIndex].quantity - 1,
      };

      if (updatedItem.quantity <= 0) {
        // Remove the item from the cart if quantity is 0 or less
        updatedCart.splice(existingItemIndex, 1);
      } else {
        // Update the item in the cart
        updatedCart[existingItemIndex] = updatedItem;
      }

      window.localStorage.setItem('cart-jng', JSON.stringify(updatedCart));
      updateCartAndStorage(updatedCart);
      return updatedCart;
    });
  }, []);

  const clearCart = useCallback(() => {
    setCart(() => {
      let updatedCart: CartItem[] = [];

      window.localStorage.setItem('cart-jng', JSON.stringify(updatedCart));
      updateCartAndStorage(updatedCart);
      return updatedCart;
    });
  }, []);

  return [addItem, cart, removeItem, clearCart];
}
